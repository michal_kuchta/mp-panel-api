<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $fields = ['email', 'password', 'name', 'address', 'personal_identity', 'phone', 'position'];

        foreach($fields as $field)
        {
            if(!$request->has($field) || empty($request->get($field)))
                return response()->json(['message' => 'Brak wymaganych danych. ('.$field.')'], 403);
        }

        try
        {
            $user = User::create([
                'email'    => $request->email,
                'password' => $request->password,
                'name' => $request->name,
                'address' => $request->address,
                'personal_identity' => $request->personal_identity,
                'phone' => $request->phone,
                'position' => $request->position,
            ]);
            $user->save();
        }
        catch (QueryException $e)
        {
            if($e->getCode() == 23000)
            {
                $mess = 'Ten adres email jest już zajęty';
            }
            else
            {
                $mess = 'Wystąpił problem podczas dodawani pracownika. Sprawdź logi';
            }

            return response()->json(['message' => $mess, 'errorCode' => $e->getCode()], 403);
        }
        catch (\Exception $e)
        {
            return response()->json(['message' => $e->getMessage()], 400);
        }

        $token = auth()->login($user);

        return $this->respondWithToken($token);
    }

    public function login()
    {
        $credentials = request(['email', 'password']);

        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['message' => 'Brak dostępu'], 401);
        }

        return $this->respondWithToken($token);
    }

    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Wylogowano pomyślnie']);
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type'   => 'bearer',
            'expires_in'   => auth()->factory()->getTTL() * 60
        ]);
    }
}
