<?php

namespace App\Http\Controllers;

use App\Client;
use App\Order;
use Illuminate\Http\Request;

class ClientsController extends Controller
{
    public function saveClient()
    {
        $post = request()->only([
            'name', 'address', 'phone'
        ]);

        if(!request()->has('name') || empty($post['name']))
            return response()->json(['message' => 'Brak nazwy klienta'], 403);

        if(!request()->has('address') || empty($post['address']))
            return response()->json(['message' => 'Brak adresu klienta'], 403);

        if(!request()->has('phone') || empty($post['phone']))
            return response()->json(['message' => 'Brak telefonu klienta'], 403);

        if(request()->has('id'))
            $client = Client::query()->findOrFail(request()->get('id'));
        else
            $client = new Client();

        $client->fill($post);
        $client->save();

        return response()->json(['message' => 'Pomyślnie zapisano dane klienta']);
    }

    public function getClient($id)
    {
        $client = Client::with(['orders'])->find(['id' => $id]);

        $client = [
            'client' => $client->toArray()
        ];

        return response()->json($client);
    }

    public function getClients($pageSize = null, $pageIndex = null)
    {
        $clients = Client::query();

        if($pageSize)
            $clients->limit($pageSize);

        if($pageIndex)
            $clients->skip($pageSize * ($pageIndex - 1 > 0 ? $pageIndex - 1 : 0));

        $clients = $clients->get();

        $clients = $clients->map(function($obj){
            return $obj->toArray();
        });

        return response()->json(['clients' => $clients]);
    }

    public function deleteClient($id)
    {
        $client = Client::query()->findOrFail($id)->delete();

	    Order::query()->where('client_id', '=', $id)->delete();

        return response()->json(['message' => 'Klient zostaą usunięty']);
    }

}
