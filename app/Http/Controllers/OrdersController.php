<?php

namespace App\Http\Controllers;

use App\Client;
use App\Order;
use Illuminate\Http\Request;

class OrdersController extends Controller
{
    public function saveOrder()
    {
        $post = request()->only([
            'make', 'model', 'plate_number', 'fuel_type', 'production_date', 'client_id', 'description', 'client_name', 'client_address', 'client_phone', 'worker_id'
        ]);

        if(!request()->has('make') || empty($post['make']))
            return response()->json(['message' => 'Brak marki samochodu'], 403);

        if(!request()->has('model') || empty($post['model']))
            return response()->json(['message' => 'Brak modelu samochodu'], 403);

        if(!request()->has('plate_number') || empty($post['plate_number']))
            return response()->json(['message' => 'Brak numeru rejestracyjnego'], 403);

        if(!request()->has('fuel_type') || empty($post['fuel_type']))
            return response()->json(['message' => 'Brak typu paliwa'], 403);

        if(!request()->has('description') || empty($post['description']))
            return response()->json(['message' => 'Brak opisu usterki'], 403);

        if(!request()->has('worker_id') || empty($post['worker_id']))
            return response()->json(['message' => 'Brak id pracownika'], 403);

        if(!request()->has('client_id'))
        {
            if(!request()->has('client_name') || empty($post['client_name']))
                return response()->json(['message' => 'Brak nazwy klienta'], 403);

            if(!request()->has('client_address') || empty($post['client_address']))
                return response()->json(['message' => 'Brak adresu klienta'], 403);

            if(!request()->has('client_phone') || empty($post['client_phone']))
                return response()->json(['message' => 'Brak telefonu klienta'], 403);

            $client = new Client();
            $client->fill(['name' => $post['client_name'], 'address' => $post['client_address'], 'phone' => $post['client_phone']]);
            $client->save();
            $post['client_id'] = $client->id;
        }
        elseif(request()->has('client_id') && empty($post['client_id']))
            return response()->json(['message' => 'Brak danych klienta'], 403);

        if(request()->has('id'))
            $order = Order::query()->whereNull('deleted_at')->findOrFail(request()->get('id'));
        else
            $order = new Order();

        $order->fill($post);
        $order->save();

        return response()->json(['message' => 'Pomyślnie zapisano dane zlecenia']);
    }

    public function getOrder($id)
    {
        $order = Order::query()->whereNull('deleted_at')->findOrFail(['id' => $id]);

        $order = [
            'order' => $order->toArray(),
        ];

        return response()->json($order);
    }

    public function getOrders($pageSize = null, $pageIndex = null)
    {
        $orders = $this->parseOrders(Order::query()->whereNull('deleted_at'), $pageSize, $pageIndex);

        return response()->json(['orders' => $orders]);
    }

    public function getHistory($pageSize = null, $pageIndex = null)
    {
        $orders = $this->parseOrders(Order::query()->whereNotNull('deleted_at'), $pageSize, $pageIndex);

        return response()->json(['orders' => $orders]);
    }

    private function parseOrders($orders, $pageSize = null, $pageIndex = null)
    {
        if($pageSize)
            $orders->limit($pageSize);

        if($pageIndex)
            $orders->skip($pageSize * ($pageIndex - 1 > 0 ? $pageIndex - 1 : 0));

        $orders = $orders->get();

        $orders = $orders->map(function($obj){
            return [
                'order' => $obj->toArray()
            ];
        });

        return $orders;
    }

    public function deleteOrder($id)
    {
        $order = Order::query()->findOrFail($id)->delete();

        return response()->json(['message' => 'Zamówienie zostało usunięte']);
    }
}
