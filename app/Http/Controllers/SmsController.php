<?php

namespace App\Http\Controllers;

use Twilio\Rest\Client;

class SmsController extends Controller
{
    public function postSend()
    {
        $post = request()->only([
            'phone_number', 'message'
        ]);

        if (!request()->has('phone_number') || empty($post['phone_number']))
            return response()->json(['message' => 'Brak numery odbiorcy'], 403);

        if (!request()->has('message') || empty($post['message']))
            return response()->json(['message' => 'Brak telefonu pracownika'], 403);

        $account_sid = env('TWILIO_SID', '');
        $auth_token = env('TWILIO_TOKEN', '');
        $twilio_number = env('TWILIO_NUMBER', '');

        if (empty($account_sid))
            return response()->json(['message' => 'Brak twilio account sid'], 403);

        if (empty($auth_token))
            return response()->json(['message' => 'Brak twilio auth token'], 403);

        if (empty($twilio_number))
            return response()->json(['message' => 'Brak twilio phone number'], 403);

        $client = new Client($account_sid, $auth_token);
        $client->messages->create(
            $post['phone_number'],
            array(
                'from' => $twilio_number,
                'body' => $post['message']
            )
        );

        return response()->json(['message' => 'Wiadomość została wysłana']);
    }
}