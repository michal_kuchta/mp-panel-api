<?php

namespace App\Http\Controllers;

use App\Client;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class WorkersController extends Controller
{
    public function saveWorker()
    {
        $post = request()->only([
            'name', 'email', 'password', 'address', 'personal_identity', 'phone', 'position'
        ]);

        if(!request()->has('name') || empty($post['name']))
            return response()->json(['message' => 'Brak nazwy pracownika'], 403);

        if(!request()->has('address') || empty($post['address']))
            return response()->json(['message' => 'Brak adresu pracownika'], 403);

        if(!request()->has('personal_identity') || empty($post['personal_identity']))
            return response()->json(['message' => 'Brak PESELu pracownika'], 403);

        if(!request()->has('phone') || empty($post['phone']))
            return response()->json(['message' => 'Brak telefonu pracownika'], 403);

        if(!request()->has('position') || empty($post['position']))
            return response()->json(['message' => 'Brak pozycji pracownika'], 403);

        if(request()->has('id'))
            $worker = User::query()->findOrFail(request()->get('id'));
        else
        {
            if(!request()->has('email') || empty($post['email']))
                return response()->json(['message' => 'Brak emaila pracownika'], 403);

            $worker = new User();
        }

        $worker->password = Hash::make(str_random(8));

        $worker->fill($post);
        $worker->save();

        return response()->json(['message' => 'Pomyślnie utworzono pracownika']);
    }

    public function getWorker($id)
    {
        $worker = User::query()->with(['orders'])->findOrFail(['id' => $id]);

        return response()->json(['worker' => $worker->toArray()]);
    }

    public function getWorkers($pageSize = null, $pageIndex = null)
    {
        $workers = User::query()->with(['orders']);

        if($pageSize)
            $workers->limit($pageSize);

        if($pageIndex)
            $workers->skip($pageSize * ($pageIndex - 1 > 0 ? $pageIndex - 1 : 0));

        $workers = $workers->get();

        $workers = $workers->map(function($obj){
            return $obj->toArray();
        });

        return response()->json(['workers' => $workers]);
    }

    public function deleteWorker($id)
    {
        $client = User::query()->with(['orders'])->findOrFail($id)->delete();

        return response()->json(['message' => 'Pracownik został usunięty']);
    }

}
