<?php

namespace App\Http\Middleware;

use Closure;

class Cors
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(isset($_SERVER['HTTP_USER_AGENT']) && preg_match('/postman/', strtolower($_SERVER['HTTP_USER_AGENT'])))
        {
            //return false;
        }

        $allowedOrigins = [
            'http://localhost:4200',
            'http://localhost:3000'
        ];

        $response = $next($request);

        if(isset($_SERVER['HTTP_ORIGIN']))
        {
            $origin = $_SERVER['HTTP_ORIGIN'];
        }
        else
        {
            $origin = 'http://localhost:3000';
        }

        if(!in_array($origin, $allowedOrigins))
        {
            $origin = 'http://localhost:3000';
        }

        $response->headers->set('Access-Control-Allow-Origin', $origin);

        $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT, DELETE');
        $response->headers->set('Access-Control-Allow-Headers', 'Content-Type, Accept, Authorization, X-Requested-With');
        $response->headers->set('Access-Control-Allow-Credentials', 'true');

        return $response;
    }
}

