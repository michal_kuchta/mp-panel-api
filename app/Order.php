<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';
    protected $fillable = [
        'make', 'model', 'plate_number', 'fuel_type', 'production_date', 'client_id', 'description', 'worker_id'
    ];

    protected $with = [
        'client'
    ];

    public function client()
    {
        return $this->hasOne(Client::class, 'id', 'client_id');
    }
}
