<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/register', 'AuthController@register');

Route::post('/login', 'AuthController@login');
Route::post('/logout', 'AuthController@logout');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => ['auth:api']], function(){
    Route::group(['prefix' => '/orders'], function(){
        Route::get('/single/{id}', 'OrdersController@getOrder');
        Route::get('/list/{pageSize?}/{pageIndex?}', 'OrdersController@getOrders');
        Route::get('/history/{pageSize?}/{pageIndex?}', 'OrdersController@getHistory');

        Route::post('/delete/{id}', 'OrdersController@deleteOrder');
        Route::post('/save', 'OrdersController@saveOrder');
    });

    Route::group(['prefix' => '/clients'], function(){
        Route::get('/single/{id}', 'ClientsController@getClient');
        Route::get('/list/{pageSize?}/{pageIndex?}', 'ClientsController@getClients');

        Route::post('/delete/{id}', 'ClientsController@deleteClient');
        Route::post('/save', 'ClientsController@saveClient');
    });

    Route::group(['prefix' => '/workers'], function(){
        Route::get('/single/{id}', 'WorkersController@getWorker');
        Route::get('/list/{pageSize?}/{pageIndex?}', 'WorkersController@getWorkers');

        Route::post('/delete/{id}', 'WorkersController@deleteWorker');
        Route::post('/save', 'WorkersController@saveWorker');
    });

    Route::group(['prefix' => '/sms'], function(){
        Route::post('/send', 'SmsController@postSend');
    });

});
